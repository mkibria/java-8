package lambdaexpression.anonymous;

public class OperateLaptop {

    public static void main(String[] args) {
        //Please implement anonymous class for interface laptop
        Laptop laptop = null;

        laptop = new Laptop() {
            @Override
            public void ramSize(int memorySize) {
                System.out.println("ram size is :"+memorySize);

            }

            @Override
            public void monitorSize(int screenSize) {
                System.out.println("screen size is :"+screenSize);
            }
        };




        laptop.monitorSize(16);
        laptop.monitorSize(15);


        //please implement anonymous function for Phone FunctionalInterface
            Phone phone = new Phone() {
                @Override
                public void cellularNetwork(String network) {
                    System.out.println("cellular carrier is :"+network);
                }
            };

            phone.cellularNetwork("Simple mobile");
            phone.cellularNetwork("T-mobile");

    }
}
