package lambdaexpression.iterator;

import java.util.Arrays;
import java.util.List;

public class IterateList {
    public static void main(String[] args) {
        List<String> electronicsList = Arrays.asList("lambda.expression.anonymous.Laptop", "Tablet", "Television", "Monitor");
        System.out.println("Classic enhanced for loop");
        for (String items : electronicsList) {
            System.out.println(items);
        }

        //Please implement Anonymous class to iterate through
        System.out.println("Using Anonymous class");
         class Elctronics1{

             public void print(List<String>eList){
                 for (String items : electronicsList) {
                     System.out.println(items);
                 }
             }
        }




        //Please implement lambda expression to iterate through electronicsList
        System.out.println("Lambda Expression");
        class Elctronics2{

            public void print(List<String>eList){
                eList.stream().forEach(System.out::println);
            }
        }


        //Please implement lambda Method Reference to iterate through electronicsList
        System.out.println("Lambda Method Reference-1");
        Electronics electronics = new Electronics();

         electronics.printList(electronicsList);


        //Please implement lambda Method Reference from Electronics class to iterate through electronicsList
        System.out.println("Lambda Method Reference-2 using Electronics class");
        Electronics electronics2 = new Electronics();
        electronics2.printList(electronicsList);


        //Please implement stream iteration through electronicsList
        System.out.println("iterate using Stream for each loop");
        electronicsList.stream().forEach(System.out::println);
    }

}

class Electronics {
    static void countWordLength(String st) {
        System.out.println(st.length());
    }
    public  void printList(List<String>list) {
        Electronic electronic = (list1) -> list1.stream().forEach(System.out::println);
        electronic.print(list);
    }


}

