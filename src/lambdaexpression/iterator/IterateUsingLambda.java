package lambdaexpression.iterator;

import java.util.HashMap;
import java.util.Map;

public class IterateUsingLambda {
    public static void main(String[] args) {
        Map<String, Integer> itemPrice = new HashMap<>();
        itemPrice.put("lambda.expression.anonymous.Laptop", 1200);
        itemPrice.put("iPhone", 650);
        itemPrice.put("AndroidPhone", 500);
        itemPrice.put("TV", 700);
        itemPrice.put("Monitor", 450);

		// Iterate without using Lambda
		 for (Map.Entry<String, Integer> entry : itemPrice.entrySet()) {
		     System.out.println("Item Name: " + entry.getKey() + " and Price: " + entry.getValue());
		 }

        // Please Implement Lambda Iteration on itemPrice map
        iterateUsingLambda(itemPrice);
        iterateUsingStreamAPI(itemPrice);



    }
    public static void iterateUsingLambda(Map<String, Integer> map) {
        map.forEach((k, v) -> System.out.println((k + ":" + v)));
    }

    public static void iterateUsingStreamAPI(Map<String, Integer> map) {
        map.entrySet().stream().forEach(e -> System.out.println(e.getKey() + ":" + e.getValue()));
    }
}
